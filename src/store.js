import Vue from 'vue'
import Vuex from 'vuex'

import router from './router'
import jwtDecode from 'jwt-decode'

Vue.use(Vuex);

const types = {
    LOGIN: 'LOGIN',
    LOGOUT: 'LOGOUT',
    ADMINON: 'ADMINON',
    ADMINOFF: 'ADMINOFF',
    USERLOGIN: 'USERLOGIN',
    USERROLE: 'USERROLE'
}

const state = {
    logged: localStorage.getItem('token'),
    admin: 0,
    login: '',
    role: ''
}

const getters = {
    isLogged: state => state.logged,
    isAdmin: state => state.admin,
    getUserLogin: state => state.login,
    getUserRole: state => state.role
}

const actions = {
    login ({commit}, credential) {

        Vue.http.post(process.env.VUE_APP_PROXY_API_ADDRESS + '/auth/login', credential)
            .then((response) => response.json())
            .then((result) => {
                localStorage.setItem('token', result.token);
                let decodeToken = jwtDecode(result.token);
                let roleToken = decodeToken.ROLE;
                let userLogin = decodeToken.LOGIN;

                commit(types.USERROLE, roleToken);
                commit(types.USERLOGIN, userLogin);

                if (roleToken === 'ADMIN') {
                    commit(types.ADMINON);
                }
                commit(types.LOGIN);
                router.push({path: '/'});
            });
    },

    logout({commit}) {
        localStorage.removeItem('token');
        commit(types.LOGOUT);
        commit(types.ADMINOFF);
        commit(types.USERLOGIN, "");
        commit(types.USERROLE, "");
        router.push({path: '/login'});
    },
}

const mutations = {
    [types.LOGIN] (state) {
        state.logged = 1;
    },

    [types.USERLOGIN] (state, userlogin){
        state.login = userlogin;
    },

    [types.USERROLE] (state, role) {
        state.role = role;
    },

    [types.LOGOUT] (state) {
        state.logged = 0;
    },

    [types.ADMINON] (state) {
        state.admin = 1;
    },

    [types.ADMINOFF] (state) {
        state.admin = 0;
    }
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})