import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from "../components/HelloWorld"
import LoginComponent from "../components/LoginComponent";
import UsersListComponent from "../components/admin/users/UsersListComponent";
import NotFoundComponent from "../components/NotFoundComponent";
import RegisterComponent from "../components/RegisterComponent";
import PasswordRecoverComponent from "../components/PasswordRecoverComponent";
import BooksComponent from "../components/BooksComponent";
import BookComponent from "../components/BookComponent";
import UserPanelComponent from "../components/user/UserPanelComponent";
import AdminPanelComponent from "../components/admin/AdminPanelComponent";
import AddUserComponent from "../components/admin/users/AddUserComponent";
import SearchUserComponent from "../components/admin/users/SearchUserComponent";
import UserProfileComponent from "../components/admin/users/UserProfileComponent";
import AddAuthorComponent from "../components/admin/authors/AddAuthorComponent";
import AuthorsComponent from "../components/AuthorsComponent";
import AuthorComponent from "../components/AuthorComponent";
import UserChangePasswordComponent from "../components/user/UserChangePasswordComponent";
import UserChangeEmailComponent from "../components/user/UserChangeEmailComponent";
import TitleBookComponent from "../components/TitleBookComponent";
import HiresListComponent from "../components/admin/hires/HiresListComponent";
import ReservationListComponent from "../components/admin/reservations/ReservationListComponent";

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'hello',
            component: HelloWorld,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/register',
            name: 'register',
            component: RegisterComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/passwordrecover',
            name: 'passwordrecover',
            component: PasswordRecoverComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/login',
            name: 'login',
            component: LoginComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/admin/users',
            name: 'userList',
            component: UsersListComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/books',
            name: 'books',
            component: BooksComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/book/:id',
            name: 'book',
            component: BookComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/account',
            name: 'account',
            component: UserPanelComponent,
            meta: {
                roles: 'USER'
            }
        },
        {
            path: '/account/changeemail',
            name: 'accountchangeemail',
            component: UserChangeEmailComponent,
            meta: {
                roles: 'USER'
            }
        },
        {
            path: '/account/changepassword',
            name: 'accountchangepassword',
            component: UserChangePasswordComponent,
            meta: {
                roles: 'USER'
            }
        },
        {
            path: '/admin',
            name: 'admin',
            component: AdminPanelComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/admin/user/add',
            name: 'adminAdd',
            component: AddUserComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/admin/user/search',
            name: 'adminSearch',
            component: SearchUserComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/admin/user/:id',
            name: 'adminUserProfile',
            component: UserProfileComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/admin/hires',
            name: 'adminListHires',
            component: HiresListComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/admin/reservations',
            name: 'adminListReservations',
            component: ReservationListComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/books/:title',
            name: 'bookTitle',
            component: TitleBookComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/admin/author/add',
            name: 'adminAuthorProfile',
            component: AddAuthorComponent,
            meta: {
                roles: 'ADMIN'
            }
        },
        {
            path: '/authors',
            name: 'authors',
            component: AuthorsComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '/author/:id',
            name: 'authorID',
            component: AuthorComponent,
            meta: {
                roles: 'GUEST'
            }
        },
        {
            path: '*',
            component: NotFoundComponent
        }

    ]
})
