import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vueResource from 'vue-resource'
import store from './store'
import Notifications from 'vue-notification'

import AsyncComputed from 'vue-async-computed'

/* Initialize the plugin */
Vue.use(AsyncComputed)
Vue.use(vueResource);
Vue.use(Notifications)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import jwtDecode from "jwt-decode";

Vue.http.interceptors.push((request, next) => {

  if(localStorage.getItem('token')) {
    request.headers.set('Authorization', 'Bearer ' + localStorage.getItem('token'));
  }

  next(response => {

    if(response.status === 401 || response.status === 403) {
      router.push({path: '/login'});
    }

  })
});

router.beforeEach((to, from, next) => {

  // eslint-disable-next-line no-console
  console.log('ref');

  if (localStorage.getItem('token')) {
    let token = localStorage.getItem('token');
    let decodeToken = jwtDecode(token);
    let roleToken = decodeToken.ROLE;
    let userLogin = decodeToken.LOGIN;

    store.commit('USERROLE', roleToken);
    store.commit('USERLOGIN', userLogin);

    if (roleToken === 'ADMIN') {
      store.commit('ADMINON');
    }
  }


  if (to.meta.roles === "GUEST" ) {
    next()
  } else if (to.meta.roles === 'ADMIN' || to.meta.roles === 'USER') {
    let token = localStorage.getItem('token');
    if (token !== null) {
      let roleToken = store.getters.getUserRole;

      if (to.meta.roles === 'ADMIN') {
        if (roleToken === 'ADMIN') {
          next()
        } else {
          next('/')
        }
      } else if (to.meta.roles === 'USER') {
        if (roleToken === 'ADMIN' || roleToken === 'USER') {
        next()
        } else {
          next('/')
        }
      }
    } else {
      next()
    }

  }

})


Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
